using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace FolioBusListenerOnPrem
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        IQueueClient queueClient;
        string ServiceBusConnectionString;
        string QueueName;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
            ServiceBusConnectionString = "Endpoint=sb://naz-sbux2.servicebus.windows.net/;SharedAccessKeyName=Roo...................E6d+8=";
            QueueName = "digitalcontent-deployment-folio";
            queueClient = new QueueClient(ServiceBusConnectionString, QueueName);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time} ", DateTimeOffset.Now);
                _logger.LogInformation("Local setting ", Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT"));
                RegisterOnMessageHandlerAndReceiveMessages();
                await Task.Delay(1000, stoppingToken);
                
            }
        }
        void RegisterOnMessageHandlerAndReceiveMessages()
        {
            var messageHandlerOptions = new MessageHandlerOptions(ExceptionReceivedHandler)
            {
                 MaxConcurrentCalls = 1,
                 AutoComplete = false
            };
            queueClient.RegisterMessageHandler(ProcessMessagesAsync, messageHandlerOptions);
        }

        static Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            Console.WriteLine($"Message handler encountered an exception {exceptionReceivedEventArgs.Exception}.");
            var context = exceptionReceivedEventArgs.ExceptionReceivedContext;
            Console.WriteLine("Exception context for troubleshooting:");
            Console.WriteLine($"- Endpoint: {context.Endpoint}");
            Console.WriteLine($"- Entity Path: {context.EntityPath}");
            Console.WriteLine($"- Executing Action: {context.Action}");
            return Task.CompletedTask;
        }

        async Task ProcessMessagesAsync(Message message, CancellationToken token)
        {
            _logger.LogInformation($"Received message body:{Encoding.UTF8.GetString(message.Body)}");
            FolioPublishNotification folioPublishNotify = new FolioPublishNotification();
            await folioPublishNotify.SendToWebhookAsync(Encoding.UTF8.GetString(message.Body), _logger);
            await queueClient.CompleteAsync(message.SystemProperties.LockToken);
        }
    }
}
