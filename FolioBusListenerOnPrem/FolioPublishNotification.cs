﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace FolioBusListenerOnPrem
{
    class FolioPublishNotification
    {
        string sbuxWebhookUrl = "https://webhook.site/1b65ecc4-83d0-40ae-a029-e9c58b899905";

        public async System.Threading.Tasks.Task SendToWebhookAsync(string FolioQueueItem, ILogger<Worker> _logger)
        {
            _logger.LogInformation($"Started sending message to WebHook URL: {FolioQueueItem}");
    
            string content = JsonConvert.SerializeObject(FolioQueueItem).ToString();
            HttpClient client = new HttpClient();
            //this line is needed when implementing access control to webhook url
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "Your Oauth token");

            var response = await client.PostAsync(sbuxWebhookUrl, new StringContent(content, Encoding.UTF8, "application/json"));
            var responseString = await response.Content.ReadAsStringAsync();
            _logger.LogInformation($"Response from webhook: Hit webhookurl {sbuxWebhookUrl} successfully with response: {responseString}");

        }
    }
}
