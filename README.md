This ReadMe provides a general idea of sending and receiving message to Azure servicebus. For both purpose `Microsoft.Azure.ServiceBus` library is used.
# Sending message
Define and initialize `IQueueClient` object: 
```C#
static  IQueueClient  queueClient;
queueClient = new  QueueClient(ServiceBusConnectionString, QueueName);
```
Send a string to the queue:
```C#
try
{
	string  messageBody = "{\"objects\":[\"/\"],\"hostname\":\"testsdlgreen.com\"}";
	var  message = new  Message(Encoding.UTF8.GetBytes(messageBody));

	await  queueClient.SendAsync(message);
}
catch (Exception  exception)
{
	Console.WriteLine($"{DateTime.Now} :: Exception: {exception.Message}");

}

}
```
Close the client:
```C#
await queueClient.CloseAsync();
```

# Reading ServiceBus Queues
Initialize and close `IQueueClient` object like before.
```C#
public  void  Main2()
{
	MainAsync2().GetAwaiter().GetResult();
}

static  async  Task  MainAsync2()
{
	queueClient = new  QueueClient(ServiceBusConnectionString, QueueName);
	RegisterOnMessageHandlerAndReceiveMessages();
	await  queueClient.CloseAsync();
}

static  void  RegisterOnMessageHandlerAndReceiveMessages()
{
	// Configure the MessageHandler Options in terms of exception handling, number of concurrent messages to deliver etc.
	var  messageHandlerOptions = new  MessageHandlerOptions(ExceptionReceivedHandler)
	{
		/* Maximum number of Concurrent calls to the callback `ProcessMessagesAsync`, set to 1 for simplicity.
		 Set it according to how many messages the application wants to process in parallel.*/
		 MaxConcurrentCalls = 1,

		/* Indicates whether MessagePump should automatically complete the messages after returning from User Callback.
		False below indicates the Complete will be handled by the User Callback as in `ProcessMessagesAsync` below.*/

		AutoComplete = false
	};

	queueClient.RegisterMessageHandler(ProcessMessagesAsync, messageHandlerOptions);
}
  
static  async  Task  ProcessMessagesAsync(Message  message, CancellationToken  token)
{
	Console.WriteLine($"Received message: SequenceNumber:{message.SystemProperties.SequenceNumber} Body:{Encoding.UTF8.GetString(message.Body)}");
	
	// Complete the message so that it is not received again. This can be done only if the queueClient is created in ReceiveMode.PeekLock mode (which is default).

	await  queueClient.CompleteAsync(message.SystemProperties.LockToken);

	// Note: Use the cancellationToken passed as necessary to determine if the queueClient has already been closed.
	// If queueClient has already been Closed, you may chose to not call CompleteAsync() or AbandonAsync() etc. calls
	// to avoid unnecessary exceptions.
}
  
static  Task  ExceptionReceivedHandler(ExceptionReceivedEventArgs  exceptionReceivedEventArgs)
{
	Console.WriteLine($"Message handler encountered an exception {exceptionReceivedEventArgs.Exception}.");
	var  context = exceptionReceivedEventArgs.ExceptionReceivedContext;
	Console.WriteLine("Exception context for troubleshooting:");
	Console.WriteLine($"- Endpoint: {context.Endpoint}");
	Console.WriteLine($"- Entity Path: {context.EntityPath}");
	Console.WriteLine($"- Executing Action: {context.Action}");
	return  Task.CompletedTask;

}
```